drop table contact;

create table contact(
id      int             not null 
        generated always as identity (start with 1, increment by 1),
nom     varchar(50)     not null, 
tel     varchar(20)     not  null,
constraint pk_contact primary key (id)
);


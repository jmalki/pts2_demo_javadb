/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demojavaappwithjavadb;

import dao.ContactDAO;
import java.sql.Connection;
import dao.DBConnection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Contact;

/**
 *
 * @author jmalki
 */
public class DemoJavaAppWithJavaDB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Test DBProperties
        DBProperties dbProps = new DBProperties(); 
        System.out.println("demo url : " + dbProps.getDbUrl());
        System.out.println("demo user : " + dbProps.getDbUser());
        System.out.println("demo passwd : " + dbProps.getDbPasswd());
        
        // Test DB Cnnnection 
        DBConnection myDemoDBConn = new DBConnection ();
        Connection conn = null;
        try {
            conn = myDemoDBConn.getConnection();
            System.out.println("Connection Established Successfull and the DATABASE NAME IS:"
                    + conn.getMetaData().getDatabaseProductName());
            myDemoDBConn.closeConnection();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
         } finally {
            if (conn != null) {
                try {
                    conn.close();
                    myDemoDBConn.closeConnection();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
        }
       
        // Test add contact 
        Contact unContact = new Contact("Jamal", "0909090909");
        ContactDAO unContactDAO = new ContactDAO();
        unContactDAO.addConact(unContact);

        // Test remove contact 
        unContactDAO.removeContact(2); 
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author jmalki
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Contact;

public class ContactDAO {

    private Connection conn;

    public ContactDAO() {
    }

    public void addConact(Contact unContact) {
        DBConnection myDemoDBConn = new DBConnection();
        Connection conn = myDemoDBConn.getConnection();
        Statement stmt = null;
        try {
            String query = "insert into contact(nom, tel) "
                    + "values ('" + unContact.getNom() + "', "
                    + "'" + unContact.getTel() + "'"
                    + ")";
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    myDemoDBConn.closeConnection();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
        }
    }

    public void removeContact(int conactid) {
        DBConnection myDemoDBConn = new DBConnection();
        Connection conn = myDemoDBConn.getConnection();
        Statement stmt = null;
        String query = "delete from contact where id = " + conactid;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            conn.commit();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    myDemoDBConn.closeConnection();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
        }
    }

    /*
    public void updatePerson(Person person) {
        String query = "update person set person.name='"
        + person.getName() + "', person.phone='" 
        + person.getPhone()
        + "', person.profession='" + person.getProfession() 
        + "' where person.idperson = " + person.getPersonId() + " ";
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Person> getPersons() throws SQLException {
        String query = "select * from person";
        ArrayList<Person> persons = new ArrayList<Person>();
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        while (res.next()) {
            Person person = new Person();
            person.setName(res.getString("name"));
            person.setPhone(res.getString("phone"));
            person.setProfession(res.getString("profession"));
            person.setPersonId(res.getInt("idperson"));
            persons.add(person);
        }
        return persons;
    }

    public Person getPersonById(int personid) throws SQLException {
        Person person = new Person();
        String query = "select * from person where person.idperson = " + personid + " ";
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        if (res.next()) {
            person.setName(res.getString("name"));
            person.setPhone(res.getString("phone"));
            person.setProfession(res.getString("profession"));
            person.setPersonId(res.getInt("idperson"));
        }
        return person;
    }
    }
     */
}
